({
    init : function (component) {
        var flow = component.find("flowData");
        flow.startFlow("screenflow1");
        var varrecordId = component.get("v.recordId");
    }, 
    handleComponentEvent : function(cmp, event) {
        var message = event.getParam("message");
        // set the handler attributes based on event data
        cmp.set("v.messageFromEvent", message);
        var numEventsHandled = parseInt(cmp.get("v.numEvents")) + 1;
        cmp.set("v.numEvents", numEventsHandled);
        $A.get("e.force:closeQuickAction").fire();
    },
    
    statusChange : function (component, event) {
        if (event.getParam('status') === "FINISHED_SCREEN") {
            var outputVarArray = event.getParam("outputVariables");
            for(let i = 0; i < outputVarArray.length; i++) {
                if(outputVarArray[i].name === 'recordId') {
                    console.log('recordId ' + outputVarArray[i].value);
                    var varrecordId = component.get("v.recordId");
                    component.set("v.varrecordId", outputVarArray[i].value);
                }
            }
            alert('finished');
            var a = component.get("v.makecall");
            component.set("v.makecall", 'makecall');
            //$A.get("e.force:closeQuickAction").fire();
        }
    }
})