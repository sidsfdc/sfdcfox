({
    init : function(component, event, helper) {
     var flow = component.find("flowData");
        flow.startFlow("floweEvents");
    },
    statusChange : function(component, event, helper) {
        if (event.getParam('status') === "FINISHED_SCREEN") {
            var outputVarArray = event.getParam("outputVariables");
            for(let i = 0; i < outputVarArray.length; i++) {
                if(outputVarArray[i].name === 'myname') {
                    console.log('myname ' + outputVarArray[i].value);
                    component.set("v.myname", outputVarArray[i].value);
                }
            }	
        }
    },
    recordUpdate : function(component, event, helper) {
        var eventParams = event.getParams();
        if (eventParams.changeType === "LOADED") {
            console.log('Name ' + component.get("v.objRecord").Name ); 
            component.set("v.getname", component.get("v.objRecord").Name);
        }
    },
    fireComponentEvent : function(component, event, helper) {
		$A.get("e.force:closeQuickAction").fire();
    },
})