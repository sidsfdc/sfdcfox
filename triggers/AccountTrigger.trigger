trigger AccountTrigger on Account(before insert) {
  for (Account acc : Trigger.new) {
    if (Trigger.isBefore && Trigger.isInsert) {
      acc.Name = acc.Name.toUpperCase();
    }
  }

}
